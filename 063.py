# Write a Python program to get an absolute file path.

def absolute_file_path(path_fname):
    import os
    return os.path.abspath('current_file')


print("Absolute file path: ", absolute_file_path("test.txt"))