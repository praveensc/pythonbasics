# Write a Python program to test whether a passed letter is a vowel or not.


def vowel_check(char):
    all_vowels = 'aeiou'
    return char in all_vowels
print(vowel_check('a'))
print(vowel_check('b'))
