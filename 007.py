# Write a Python program to accept a filename from the user and print the
# extension of that.
# Sample filename : abc.java
# Output : java

def file_ext():
    filename = input("Input the Filename: ")
    f_extns = filename.split(".")
    print ("The extension of the file is : " + repr(f_extns[-1]))

file_ext()