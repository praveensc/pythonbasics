# Write a Python program to solve (x + y) * (x + y).


x, y = 2, 4
result = x * x + 2 * x * y + y * y  # formula {(a+b)^2=a^+2ab+b^}
print("({} + {}) ^ 2) = {}".format(x, y, result))