# Write a Python program to sum of two given integers. However, if the sum is
# between 15 to 20 it will return 20.

def sum2(x,y):
    sum=x+y
    if sum in range(15,20):
        return 20
    else:
        return sum

print(sum2(4,7))
print(sum2(15,3))
print(sum2(15,4))
print(sum2(14,2))
print(sum2(11,2))