# Write a Python program to display your details like name, age, address in
# three different lines.


def my_details():
    name, age = "praveen", 23
    address = "Bangalore, Karnataka, India"
    print("Name: {}\nAge: {}\nAddress: {}".format(name, age, address))

my_details()
