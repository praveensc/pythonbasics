# Write a Python program which accepts the radius of a circle from the user and
# compute the area.
# Sample Output :
# r = 1.1
# Area = 3.8013271108436504

def Area_circle():
    r=int(input("Enter the radius for circle:"))
    area=3.14*r*r
    print(area)

Area_circle()