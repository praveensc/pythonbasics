# Write a Python program to get the volume of a sphere with radius 6.

def area_sphere(r):
    PI = 3.1415926535897931
    # r= 5
    V= 4/3*PI* r**3
    print("The volume of the sphere is: ",V)


r=int(input("Enter the radius:"))
area_sphere(r)