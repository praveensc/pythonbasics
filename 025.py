# Write a Python program to check whether a specified value is contained in a
# group of values.


def check_value(group,n):
    for value in group:
        if n==value:
            return True
    return False

print(check_value([1,3,5,7],8))
print(check_value([1,3,5,7],3))

